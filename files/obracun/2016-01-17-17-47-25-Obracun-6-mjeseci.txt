Obračun za 6 mjeseci baziran na računu za rasvjetu od 300.0 kuna i računu za lift od 50.0 kuna.

| Stan ID   | Ime Vlasnika   | Prezime Vlasnika   |   Broj Stanara |   Kat |      Dug |   Za uplatu |
|-----------+----------------+--------------------+----------------+-------+----------+-------------|
| stan1     | Ana            | Anic               |              1 |    -1 |    0     |          89 |
| stan2     | Biba           | Bibiđ              |              2 |     1 | -175.406 |         376 |
| stan3     | Didi           | Didić              |              3 |     2 |    0     |           0 |
| stan4     | Ele            | Eliš               |              4 |     0 | -474.278 |         875 |
| stan5     | Tomi           | Tomič              |              6 |     3 |    0     |         753 |
| stan6     | Riba           | Ribiž              |              2 |     5 | -650.692 |         906 |