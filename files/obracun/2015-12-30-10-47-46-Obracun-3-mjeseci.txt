| Stan ID   | Ime Vlasnika   | Prezime Vlasnika   |   Broj Stanara |   Kat |   Dug |   Za uplatu |
|-----------+----------------+--------------------+----------------+-------+-------+-------------|
| stan1     | Ana            | Anic               |              1 |    -1 |     0 |           0 |
| stan2     | Biba           | Bibiđ              |              2 |     1 |     0 |           0 |
| stan3     | Didi           | Didić              |              3 |     2 |     0 |          76 |
| stan4     | Ele            | Eliš               |              4 |     0 |     0 |          63 |
| stan5     | Tomi           | Tomič              |              6 |     3 |     0 |         144 |
| stan6     | Riba           | Ribiž              |              2 |     5 |     0 |           0 |