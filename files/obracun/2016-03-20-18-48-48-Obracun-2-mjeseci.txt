Obračun za 2 mjeseci baziran na računu za rasvjetu od 123.0 kuna i računu za lift od 12.0 kuna.

| Stan ID   | Ime Vlasnika   | Prezime Vlasnika   |   Broj Stanara |   Kat |       Dug |   Za uplatu |
|-----------+----------------+--------------------+----------------+-------+-----------+-------------|
| stan1     | Ana            | Anic               |              1 |    -1 |     0     |           0 |
| stan2     | Biba           | Bibiđ              |              2 |     1 |  -651.372 |         679 |
| stan3     | Didi           | Didić              |              3 |     2 | -1586.46  |        1635 |
| stan4     | Ele            | Eliš               |              4 |     0 | -2779.28  |        2834 |
| stan5     | Tomi           | Tomič              |              6 |     3 | -1110.7   |        1206 |
| stan6     | Riba           | Ribiž              |              2 |     5 | -1305.54  |        1338 |