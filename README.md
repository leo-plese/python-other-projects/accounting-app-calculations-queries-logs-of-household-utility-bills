# Accounting App Calculations Queries Logs of Household Utility Bills 

High school project in Computer Science. Making a Python app for a practical use of keeping accounts of all the co-owerns of apartments in a building, which support making calculations of co-owners' debts for a time period, querying and logging the calculations etc.

Implemented in Python.

Created: 2015-2016