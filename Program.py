# -*- coding: utf-8 -*-
# v1.12
# sljedeca import oznacava unicode znakove kod inputa http://stackoverflow.com/questions/809796/any-gotchas-using-unicode-literals-in-python-2-6
from __future__ import unicode_literals
# sljedeći import omogućava dobavljanje patha ove datoteke (vidi defMain) http://stackoverflow.com/questions/21005822/what-does-os-path-abspathos-path-joinos-path-dirname-file-os-path-pardir
import os
# sljedeci import treba za enkodiranje unicode znakova pri čitanju txt filea http://stackoverflow.com/questions/491921/unicode-utf8-reading-and-writing-to-files-in-python
import codecs
# sljedeci import treba za metodu ceil
from math import ceil
# tocno vrijeme (treba u funkcijama spremiLogObracuna i spremiLog http://stackoverflow.com/questions/415511/how-to-get-current-time-in-python
from time import strftime
# trebalo je za instalaciju modula tabulate potrebnog za izlist stanova u funkciji ispisi https://pypi.python.org/pypi/tabulate 
#u python 3.5 instalacija modula 'sudo pip3 install tabulate' testirano na OS X 10.9 - ovime se mogu kreirati tablice teksta
# tabele https://pypi.python.org/pypi/tabulate
from tabulate import tabulate



#defUcitaj - ucitavanje podataka iz datoteke PopisStanara.txt u listu lstGlbStanovi za daljnju obradu
def defUcitaj():
    global lstGlbListaStanova 
    global lstGlbStanovi

    #učitavanje sadržaja datoteke PopisStanara.txt u string strFile
    strFile = codecs.open(strGlbScrptpth + '/files/PopisStanara.txt', encoding='utf-8')
    
    #u listu lstGlbListaStanova se učitavaju linije iz datoteke PopisStanara.txt
    #tipovi podataka unutar lstGlbListaStanova: [str(Stan ID), str(Ime Vlasnika), str(Prezime Vlasnika), str(Broj Stanara), str(Kat), str(Stanje racuna)]
    lstGlbListaStanova = []
    lstGlbListaStanova = strFile.readlines()
    strFile.close()
    
    #u listu lstGlbStanovi se učitavaju elementi liste lstGlbListaStanova u zapisu podataka o stanovima odvojenih s ';'
    #tipovi podataka unutar liste lstGlbStanovi: [str(Stan ID), str(Ime Vlasnika), str(Prezime Vlasnika), int(Broj Stanara), int(Kat), float(Stanje racuna)]
    lstGlbStanovi = [[] for i in range(len(lstGlbListaStanova))]
    for i in range(0, len(lstGlbListaStanova)):
        lstGlbStanovi[i].append(lstGlbListaStanova[i].split(';')[0])
        lstGlbStanovi[i].append(lstGlbListaStanova[i].split(';')[1])
        lstGlbStanovi[i].append(lstGlbListaStanova[i].split(';')[2])
        lstGlbStanovi[i].append(int(lstGlbListaStanova[i].split(';')[3]))
        lstGlbStanovi[i].append(int(lstGlbListaStanova[i].split(';')[4]))
        lstGlbStanovi[i].append(float(lstGlbListaStanova[i].split(';')[5]))
        

#defSpremi - upis/spremanje podataka iz lstGlbStanovi u datoteku PopisStanara.txt nakon obračuna računa i uplate novca
def defSpremi():
    #strTxtpth - path datoteke PopisStanara.txt
    strTxtpth = strGlbScrptpth + '/files/PopisStanara.txt'
    
    #učitavanje sadržaja datoteke PopisStanara.txt u string strPopis; za upisivanje/write
    strPopis = codecs.open(strTxtpth, 'w', encoding='utf-8')
    
    #upis podataka iz liste lstGlbListaStanova u datoteku PopisStanara.txt
    for i in range(0, len(lstGlbListaStanova)):
        strWrite = str(lstGlbStanovi[i][0]) + ';' + lstGlbStanovi[i][1] + ';' + lstGlbStanovi[i][2] + ';' + str(lstGlbStanovi[i][3]) + ';' + str(lstGlbStanovi[i][4]) + ';' + str(lstGlbStanovi[i][5]) + '\n'
        strPopis.write(strWrite)
    print(' ')
    strPopis.close()


#defSpremiLogObracuna - dodavanje novog loga obračuna; upis/spremanje podataka iz lstGlbStanovi u datoteku LogObracuna.txt prije i poslije obračuna računa i uplate novca
def defSpremiLogObracuna():
    #strTxtpth - path datoteke LogObracuna.txt
    strTxtpth = strGlbScrptpth + '/files/LogObracuna.txt'
    
    #učitavanje sadržaja datoteke LogObracuna.txt u string strPopis; za dodavanje/append
    strPopis = codecs.open(strTxtpth, 'a', encoding = 'utf-8')
    
    #učitavanje vremena obračuna u string strVrijemeZapisa
    strVrijemeZapisa = strftime('%Y-%m-%d %H:%M:%S')
    
    #učitavanje podataka iz liste lstGlbListaStanova i vremena obračuna strVrijemeZapisa u datoteku LogObracuna.txt
    for i in range(0, len(lstGlbListaStanova)):
        strPopis.write(lstGlbStanovi[i][0] + ';' + lstGlbStanovi[i][1] + ';' + lstGlbStanovi[i][2] + ';' + str(lstGlbStanovi[i][3]) + ';' + str(lstGlbStanovi[i][4]) + ';' + str(lstGlbStanovi[i][5]) + ';' + strVrijemeZapisa + '\n')
    strPopis.close()


#defSpremiLog - spremanje loga restorea popisa stanara, obračuna računa za rasvjetu i lift, uplate novca i salda blagajne u zasebnu datoteku Log.txt
def defSpremiLog(strOpis, strSvota):
    #strTxtpth - path datoteke Log.txt
    strTxtpth = strGlbScrptpth + '/files/Log.txt'
    
    #učitavanje sadržaja datoteke Log.txt u string strPopis; za dodavanje/append
    strPopis = codecs.open(strTxtpth, 'a', encoding = 'utf-8')
    
    #učitavanje vremena obračuna u string strVrijemeZapisa
    strVrijemeZapisa = strftime('%Y-%m-%d %H:%M:%S')
    
    #upis vremena zapisa strVrijemeZapisa, opisa loga (obračun računa struja/lift, uplata novca, saldo blagajne) strOpis i svote strSvota u datoteku strPopis
    strPopis.write(strVrijemeZapisa + ';' + strOpis +  ';' + strSvota + '\n')
    strPopis.close()


#defBackupPopisStanara - backup popisa stanara prije obračuna računa i uplate novca, zapisuje se u zasebnu datoteku
def defBackupPopisStanara():
    global strGlbVrijemeZapisaBak
    #učitavanje vremena backupa u string strVrijemeZapisa
    strGlbVrijemeZapisaBak = strftime('%Y-%m-%d-%H-%M-%S')
    
    #strTxtpth - path datoteke backupa (u sebi sadrži vrijeme backupa strGlbVrijemeZapisaBak)
    strTxtpth = strGlbScrptpth + '/files/backup/' + strGlbVrijemeZapisaBak + '-PopisStanara.txt'
    
    #učitavanje sadržaja datoteke backupa u string strPopis; za upisivanje/write
    strPopis = codecs.open(strTxtpth, 'w', encoding = 'utf-8')
    
    #učitavanje podataka iz liste lstGlbListaStanova u datoteku backupa
    for i in range(0, len(lstGlbListaStanova)):
        strWrite = lstGlbStanovi[i][0] + ';' + lstGlbStanovi[i][1] + ';' + lstGlbStanovi[i][2] + ';' + str(lstGlbStanovi[i][3]) + ';' + str(lstGlbStanovi[i][4]) + ';' + str(lstGlbStanovi[i][5]) + '\n'
        strPopis.write(strWrite)
    strPopis.close()


#defRestorePopisStanara - restore/vraćanje popisa stanara na prethodno stanje pomoću prijepisa iz zadnje datoteke backupa
def defRestorePopisStanara():
    print('')
    #ako je strGlbVrijemeZapisaBak != '0', to znači da je postoji već prethodno stanje za povratak odnosno prošao je već barem 1 backup gdje ono poprima određeno vrijeme backupa
    if strGlbVrijemeZapisaBak != '0':
        #ako je u sljedecem pitanju odgovoreno sa d (da) onda se krece u restore zadnjeg backupa
        strRestoreDaNe = input('Jeste li sigurni da želite povratak na prethodno stanje? d/n ')

        if strRestoreDaNe == 'd':
            #upis akcije restorea u log pozivom funkcije defSpremiLog koja to sprema u datoteku Log.txt
            defSpremiLog('Storno zadnje transakcije','0')
            
            #strTxtpthR - path datoteke backupa
            strTxtpthR = strGlbScrptpth + '/files/backup/' + strGlbVrijemeZapisaBak + '-PopisStanara.txt'
            
            #strTxtpthW - path datoteke PopisStanara.txt
            strTxtpthW = strGlbScrptpth + '/files/PopisStanara.txt'
            
            #učitavanje sadržaja datoteke PopisStanara.txt u string strPopisW; za upisivanje/write
            strPopisW = codecs.open(strTxtpthW, 'w', encoding = 'utf-8')

            #učitavanje sadržaja datoteke backupa u string strPopisR; za čitanje/read
            strPopisR = codecs.open(strTxtpthR, 'r', encoding = 'utf-8')
            
            #upis pročitane datoteke backupa u datoteku PopisStanara.txt
            strPopisW.write(strPopisR.read())
            strPopisR.close()
            strPopisW.close()
            print('Povratak podataka izvršen!')
            
        else:
            print('Nije izvršen restore.')
            
    #inače, ne postoji prethodno stanje za povratak odnosno nije prošao još nijedan backup pa je stanje strGlbVrijemeZapisaBak ostalo na svojoj početnoj vrijednosti '0'
    else:
        print('Ne postoji prethodno stanje za povratak!')
    print('')
    defMenu()


#defIspisi - ispis stanova sa svim podacima; ispis na ekran se izvršava automatski poslije obračuna računa te prije i poslije uplate novca
def defIspisi():
    defUcitaj()
    print ('----------------')
    print (' Izlist stanova')
    print ('----------------')
    #tabelarno organiziran ispis stanova sa svim pripadnim podacima 
    print (tabulate(lstGlbStanovi, headers=['Stan ID', 'Ime Vlasnika', 'Prezime Vlasnika', 'Broj Stanara', 'Kat', 'Stanje Racuna'], tablefmt='orgtbl'))

    
#defBrojStanara - izračun broja stanara koji plaćaju rasvjetu i koji plaćaju lift
def defBrojStanara():
    global intGlbStanariRasvjeta
    global intGlbStanariLift
    
    #varijabla intGlbStanariRasvjeta spremit će br. stanara koji plaćaju rasvjetu; ovdje se inicijalizira na vrijednost 0
    intGlbStanariRasvjeta = 0
    
    #varijabla intGlbStanariLift spremit će br. stanara koji plaćaju lift; ovdje se inicijalizira na vrijednost 0
    intGlbStanariLift = 0
    
    #sumiranje stanara i upis u varijable sume za lift i rasvjetu
    for i in range(0,len(lstGlbListaStanova)):
        #pribrajanje br. stanara u određenom stanu u intGlbStanariRasvjeta
        intGlbStanariRasvjeta += lstGlbStanovi[i][3]
        
        #uz uvjet da je stan na katu višem od 1. -> pribrajanje br. stanara u određenom stanu u intGlbStanariLift
        if lstGlbStanovi[i][4] > 1:
            intGlbStanariLift += lstGlbStanovi[i][3]


#defUpisRacuna - upis računa za rasvjetu odnosno lift
def defUpisRacuna(strRorL):
    #strRorL - opcija upisa računa za rasvjetu 'R'
    if strRorL == 'R':
        #upis računa rasvjete u fltRacStruja - poziva se funkcija defInputBroj koja konvertira str u float
        fltRacStruja = defInputBroj('f', 'Upiši račun struje rasvjete Kn: ')
        
        #ako fltRacStruja != 0, to znači da je unos bio ispravan pa je program vratio istu konvertiranu u float
        if fltRacStruja != 0:
            #poziv funkcije defObracunRacuna s izračunatom svotom za uplatu 
            defObracunRacuna('R', fltRacStruja)
        #fltRacStruja == 0 zbog nevaljanog unosa
        else:
            print('Niste upisali valjanu vrijednost. Obračun se nije izvršio.')
        
    #strRorL - opcija upisa računa za lift 'L'
    elif strRorL == 'L':
        #upis računa lifta u fltRacStruja
        fltRacStruja = defInputBroj('f', 'Upiši račun struje lifta Kn: ')
        
        #ako fltRacStruja != 0, to znači da je unos bio ispravan pa je program vratio istu konvertiranu u float
        if fltRacStruja != 0:
            #poziv funkcije defObracunRacuna s izračunatom svotom za uplatu 
            defObracunRacuna('L', fltRacStruja)
        #fltRacStruja == 0 zbog nevaljanog unosa
        else:
            print('Niste upisali valjanu vrijednost. Obračun se nije izvršio.')
            
    defMenu()

    
#defObracunRacuna - obračun računa
def defObracunRacuna(strRorL, fltRacunSvota):
    #poziv funkcije defSpremiLogObracuna radi spremanja loga obračuna u datoteku LogObracuna.txt
    defSpremiLogObracuna()
    
    #obračun računa za rasvjetu
    if strRorL == 'R':
        #poziv funkcije defBackupPopisStanara za backup prije obračuna računa za rasvjetu
        defBackupPopisStanara()
        
        #iznos po 1 stanaru = ukupna svota / ukupni br. stanara za rasvjetu
        fltKnPoStanaru = fltRacunSvota / intGlbStanariRasvjeta
        
        #poziv funkcije defSpremiLog radi spremanja loga obračuna rasvjete u datoteku Log.txt
        defSpremiLog('Račun struje stubišne rasvjete (' + str(intGlbStanariRasvjeta) + ' stanara, po stanaru ' + str(fltKnPoStanaru) + ')', str(fltRacunSvota))
        
        #for petlja ide po svakom stanu (elementu liste lstGlbListaStanova) - izračun potrebne uplate
        for i in range(0, len(lstGlbListaStanova)):
            #svota za uplatu = sadašnje stanje - (br. stanara u stanu * iznos po 1 stanaru)
            lstGlbStanovi[i][5] = lstGlbStanovi[i][5] - (lstGlbStanovi[i][3] * fltKnPoStanaru)
            
    #obračun računa za lift
    elif strRorL == 'L':
        #poziv funkcije defBackupPopisStanara za backup prije obračuna računa za lift
        defBackupPopisStanara()
        
        #iznos po 1 stanaru = ukupna svota / ukupni br. stanara za lift
        fltKnPoStanaru = fltRacunSvota / intGlbStanariLift
        
        #poziv funkcije defSpremiLog radi spremanja loga obračuna lifta u datoteku Log.txt
        defSpremiLog('Račun struje lifta (' + str(intGlbStanariLift) + ' stanara, po stanaru ' + str(fltKnPoStanaru) + ')', str(fltRacunSvota))
        
        #for petlja ide po svakom stanu (elementu liste lstGlbListaStanova) - izračun potrebne uplate
        for i in range(0, len(lstGlbListaStanova)):
            #provjera je li stan na katu višem od 1.
            if lstGlbStanovi[i][4] > 1:
                
                #svota za uplatu = sadašnje stanje - (br. stanara u stanu * iznos po 1 stanaru)
                lstGlbStanovi[i][5] = lstGlbStanovi[i][5] - (lstGlbStanovi[i][3] * fltKnPoStanaru)
                
    #poziv funkcije defSpremiLogObracuna radi spremanja loga obračuna u datoteku LogObracuna.txt
    defSpremiLogObracuna()
    
    #poziv funkcije defSpremi radi spremanja popisa stanara s novim svotama za uplatu u datoteku PopisStanara.txt
    defSpremi()
    
    #ispis stanova sa svim pripadnim podacima (prikaz stanja računa poslije obračuna)
    defIspisi()
    

#defIzlist - ispis stanova koji se izvršava kao 3. opcija iz menua (nakon toga poziva menu)
def defIzlist():
    #poziv funkcije defIspisi za ispis stanova sa svim pripadnim podacima
    defIspisi()
    defMenu()
    

#defUplataNovca - uplata novca na račun određenog stana
def defUplataNovca():
    #poziv funkcije defIspisi za ispis stanova sa svim pripadnim podacima (prikaz stanja računa prije uplate novca)
    defIspisi()
    
    #upis stan ID-a na koji se želi uplatiti svota
    strStanID = input('Upišite stan ID: ')
    
    intStanZaUplatu = -1
    
    #for petlja ide po svim stanovima radi provjere postojanja stan ID-a upisanog u strStanID
    for i in range(0, len(lstGlbListaStanova)):
        #ako upisani stan ID odgovara, to je potvrda da on postoji pa se intStanZaUplatu dobiva novu vrijednost (r.br. stana)  
        if lstGlbStanovi[i][0] == strStanID: intStanZaUplatu = i

    #ako je intStanZaUplatu == -1, to znači da je zadržana početna vrijednost tj. stan ID ne postoji
    if intStanZaUplatu == -1:
        print('Nepostojeći ID stana.')
    else:
        #poziv funkcije defBackupPopisStanara radi backupa podataka (spremanja stanja prije uplate)
        defBackupPopisStanara()
        print('Trenutno stanje racuna stana ' + lstGlbStanovi[intStanZaUplatu][0] + ' je: ' + str(lstGlbStanovi[intStanZaUplatu][5]))
        
        #poziv funkcije defSpremiLogObracuna radi spremanja loga obračuna u datoteku LogObracuna.txt
        defSpremiLogObracuna()
        
        #upis željene svote za uplatu
        fltSvotaZaUplatu = defInputBroj('f', 'Upišite svotu za uplatu: ')

        #ako fltSvotaZaUplatu != 0, to znači da je unos bio ispravan pa je program vratio istu konvertiranu u float
        if fltSvotaZaUplatu != 0:
            #pribrajanje fltSvotaZaUplatu - svote za uplatu na stanje računa željenog stana
            lstGlbStanovi[intStanZaUplatu][5] += fltSvotaZaUplatu
            print('Novo stanje računa stana ' + lstGlbStanovi[intStanZaUplatu][0] + ' je: ' + str(lstGlbStanovi[intStanZaUplatu][5]))
            
            #poziv funkcije defSpremiLog radi spremanja loga uplate novca u datoteku Log.txt
            defSpremiLog('Uplaćeno na račun stana ' + lstGlbStanovi[intStanZaUplatu][0], str(fltSvotaZaUplatu))
            
            #poziv funkcije defBackupPopisStanara radi backupa podataka (spremanja stanja poslije uplate)
            defSpremiLogObracuna()
            
            #poziv funkcije defSpremi radi spremanja popisa stanara s promijenjenom svotom stana u datoteku PopisStanara.txt
            defSpremi()
            
            #ispis stanova sa svim pripadnim podacima (prikaz stanja računa poslije uplate novca)
            defIspisi()

        #fltSvotaZaUplatu == 0 zbog nevaljanog unosa
        else:
            print('Niste upisali valjanu vrijednost. Uplata se nije izvršila.')
                   
    defMenu()


#defSaldoBlagajne - ispis i spremanje u log salda blagajne
def defSaldoBlagajne():
    fltSaldo = 0
    for i in range(0, len(lstGlbListaStanova)):
        #dodavanje stanja računa u saldo blagajne
        fltSaldo += lstGlbStanovi[i][5]
    print('')
    print('Saldo blagajne je: ' + str(fltSaldo))
    print('')
    
    #poziv funkcije defSpremiLog radi zapisa salda blagajne u datoteku Log.txt 
    defSpremiLog('Saldo blagajne ' + str(fltSaldo), str(fltSaldo))
    defMenu()


#defObracunUplataZaXMjeseci - predviđanje svota za uplatu ###za sljedećih X mjeseci uzimajući u obzir zadnji račun struje rasvjete/lifta, ukupan br. stanara za rasvjetu/lift, br. stanara po stanu i trenutno stanje računa stana
def defObracunUplataZaXMjeseci():
    #upis željenog broja mjeseci za obračun
    intObracunXMjeseci = defInputBroj('i', 'Za koliko mjeseci želite obračun: ')

    #ako intObracunXMjeseci != 0, to znači da je unos bio ispravan pa je program vratio istu konvertiranu u int
    if intObracunXMjeseci != 0:
        #upis zadnje svote računa rasvjete
        fltObracunXRasvjeta = defInputBroj('f', 'Upišite zadnji račun stubišne rasvjete: ')
        
        #ako fltObracunXRasvjeta != 0, to znači da je unos bio ispravan pa je program vratio istu konvertiranu u float
        if fltObracunXRasvjeta != 0:
            #upis zadnje svote računa lifta
            fltObracunXLift = defInputBroj('f', 'Upišite zadnji račun lifta: ')
            
            #ako fltObracunXLift != 0, to znači da je unos bio ispravan pa je program vratio istu konvertiranu u float
            if fltObracunXLift != 0:
                #lista lstObracunUplataX služi za dodavanje potrebnih svota za uplatu
                #tipovi podataka unutar liste fltObracunXLift: [str(Stan ID), str(Ime Vlasnika), str(Prezime Vlasnika), int(Broj Stanara), int(Kat), float(Dug), float(Za Uplatu (dug+novo))]
                lstObracunUplataX = [[] for i in range(len(lstGlbListaStanova))]
                
                for i in range(0, len(lstGlbListaStanova)):
                    fltObracunX = 0
                    #dodavanje informacija o stanu i njegovim stanarima u listu lstObracunUplataX
                    lstObracunUplataX[i].append(lstGlbStanovi[i][0])
                    lstObracunUplataX[i].append(lstGlbStanovi[i][1])
                    lstObracunUplataX[i].append(lstGlbStanovi[i][2])
                    lstObracunUplataX[i].append(lstGlbStanovi[i][3])
                    lstObracunUplataX[i].append(lstGlbStanovi[i][4])
                    
                    #ako je račun u minusu, onda se piše stanje u polje dug, inače 0
                    if lstGlbStanovi[i][5] < 0:
                        lstObracunUplataX[i].append(lstGlbStanovi[i][5])
                    else:
                        lstObracunUplataX[i].append('0')
                        
                    #obračun računa rasvjete za X mjeseci = ((svota zadnjeg računa rasvjete / br. starnara za rasvjetu) * br. mjeseci) * br. stanara u stanu
                    fltObracunX = ((fltObracunXRasvjeta / intGlbStanariRasvjeta) * intObracunXMjeseci) * lstGlbStanovi[i][3]
                    #ako je stan na katu višem od 1., onda se u svotu obračuna dodaje i obračun lifta za X mjeseci
                    if lstGlbStanovi[i][4] > 1:
                        #dodavanje obračuna za lift u ukupnu svotu obračuna
                        fltObracunX += ((fltObracunXLift / intGlbStanariLift) * intObracunXMjeseci) * lstGlbStanovi[i][3]
                        
                    #ako je izračunata svota obračuna veća od stanja računa, onda se izračunava svota za uplatu = svota obračuna - stanje računa, inače se dodaje 0 (nije potrebno ništa platiti)
                    if fltObracunX > lstGlbStanovi[i][5]:
                        fltObracunX = fltObracunX - lstGlbStanovi[i][5]
                        #svota za uplatu se zaokružuje na veći cijeli broj kn
                        lstObracunUplataX[i].append(ceil(fltObracunX))
                    else:
                        lstObracunUplataX[i].append('0')
                        
                #upis vremena obračuna u strVrijemeObracuna
                strVrijemeObracuna = strftime('%Y-%m-%d-%H-%M-%S')
                
                #strTxtpth - path datoteke obračuna (sadržava vrijeme obračuna)
                strTxtpth = strGlbScrptpth + '/files/obracun/' + strVrijemeObracuna + '-Obracun-' + str(intObracunXMjeseci) + '-mjeseci.txt'
                
                #učitavanje sadržaja datoteke obračuna u string strObracunX; za upisivanje/write
                strObracunX = codecs.open(strTxtpth, 'w', encoding = 'utf-8')
                
                #upis obračuna i tabelarno organiziranog popisa stanara u datoteku obračuna
                strObracunX.write('Obračun za ' + str(intObracunXMjeseci) + ' mjeseci baziran na računu za rasvjetu od ' + str(fltObracunXRasvjeta) + ' kuna i računu za lift od ' + str(fltObracunXLift) + ' kuna.')
                strObracunX.write('\n\n')
                strObracunX.write(tabulate(lstObracunUplataX, headers=['Stan ID', 'Ime Vlasnika', 'Prezime Vlasnika', 'Broj Stanara', 'Kat', 'Dug', 'Za uplatu'], tablefmt='orgtbl'))
                strObracunX.close()
                
                #ispis obavijesti o obračunu i tabelarno organizirani ispis podataka o stanovima
                print('')
                print('Obračun za ' + str(intObracunXMjeseci) + ' mjeseci baziran na računu za rasvjetu od ' + str(fltObracunXRasvjeta) + ' kuna i računu za lift od ' + str(fltObracunXLift) + ' kuna.')
                print('')
                print (tabulate(lstObracunUplataX, headers=['Stan ID', 'Ime Vlasnika', 'Prezime Vlasnika', 'Broj Stanara', 'Kat', 'Dug', 'Za uplatu'], tablefmt='orgtbl'))
                print('')
                
            #fltObracunXLift == 0 zbog nevaljanog unosa
            else:
                print('Niste upisali valjanu vrijednost. Obračun se nije izvršio.')
                
        #fltObracunXRasvjeta == 0 zbog nevaljanog unosa
        else:
            print('Niste upisali valjanu vrijednost. Obračun se nije izvršio.')
                
    #intObracunXMjeseci == 0 zbog nevaljanog unosa
    else:
        print('Niste upisali valjanu vrijednost. Obračun se nije izvršio.')
        
    defMenu()


#defMinusRacun - ispis računa koji su u minusu s njihovim stanjima računa svaki puta prije ispisa menua
def defMinusRacun():
    #poziv funkcije defUcitaj radi učitavanja podataka o stanovima
    defUcitaj()
    
    #lista lstMinusi služi za dodavanje stanova sa stanjem računa u minusu
    #tipovi podataka unutar liste lstMinusi: [str(Stan ID), str(Ime Vlasnika), str(Prezime Vlasnika), float(Stanje Racuna)]
    lstMinusi = [[] for i in range(len(lstGlbListaStanova))]
    
    #inicijalizacija brojača stanova s računom u minusu na 0
    intBrojac = 0

    for i in range(0, len(lstGlbListaStanova)):
        #ako je račun u minusu onda upisuje u listu lstMinusi potrebne podatke
        if lstGlbStanovi[i][5] < 0:
            lstMinusi[intBrojac].append(lstGlbStanovi[i][0])
            lstMinusi[intBrojac].append(lstGlbStanovi[i][1])
            lstMinusi[intBrojac].append(lstGlbStanovi[i][2])
            lstMinusi[intBrojac].append(lstGlbStanovi[i][5])
            #brojač pribraja stan u minusu
            intBrojac += 1
            
    #izvršava ako postoji stan u minusu što je zabilježeno u brojaču koji je tad veći od 0
    if intBrojac > 0:
        print('')
        print('Računi sa saldom u minusu !!!')
        print ('-----------------------------')
        #ispis stanova u minusu sa stanjem računa
        for i in range(0, intBrojac):
            print('U minusu je stan ' + lstMinusi[i][0] + ', vlasnika ' + lstMinusi[i][1] + ' ' + lstMinusi[i][2] +  ' sa stanjem računa ' + str(lstMinusi[i][3]))
        print ('-----------------------------')

        
#defKrajRada - obavijest korisniku o izlazu iz programa
def defKrajRada():
    print('')
    print ('-------------------------')
    print(' Izašli ste iz programa.')
    print ('-------------------------')


#defMenu - menu - odabir opcije tj. željene akcije
def defMenu():
    #poziv funkcije defMinusRacun radi obavijesti korisniku o stanovima sa saldom u minusu 
    defMinusRacun()
    
    #ispis menua s mogućim opcijama
    print('')
    print ('------')
    print(' Menu')
    print ('------')
    print('1 - Upiši račun struje stubišne rasvjete')
    print('2 - Upiši račun struje lifta') 
    print('3 - Izlist stanova')
    print('4 - Uplata novca')
    print('5 - Saldo blagajne')
    print('6 - Obračun za uplatu za X mjeseci')
    print('7 - Povratak na prethodno stanje')
    print('8 - Kraj rada')
    #dict options da se pozove funkcija s parametrom http://stackoverflow.com/questions/24580993/calling-functions-with-parameters-using-a-dictionary-in-python
    #upis broja željene opcije
    strO = input('Odaberi opciju: ')
    
    #traži upis sve dok nije ispravan (mora biti jedan od ponuđenih br. opcija)
    while strO not in '12345678':
        strO = input('Odaberi opciju: ')
        
    #rječnik dctOptions definira sadržaj opcija iz menua
    #tipovi podataka unutar rječnika dctOptions: {str(br. opcije) : tuple(ime funkcije, ulazni parametar funkcije)}
    dctOptions = {'1' : (defUpisRacuna, 'R'),
            '2' : (defUpisRacuna, 'L'),
            '3' : (defIzlist, ''),
            '4' : (defUplataNovca, ''),
            '5' : (defSaldoBlagajne, ''),
            '6' : (defObracunUplataZaXMjeseci, ''),
            '7' : (defRestorePopisStanara, ''),
            '8' : (defKrajRada, '')}
    #ako '' stoji na 1. mjestu tuplea u value rječnika, funkcija na 0. mjestu tuplea se poziva se bez ulaznih argumenata, inače se uzima navedeni parametar s istog mjesta
    if dctOptions[strO][1] == '':
        dctOptions[strO][0]()
    else:
        dctOptions[strO][0](dctOptions[strO][1])

#Tata mi je objasnio da je exception handling važan dio programa pa sam ga ja primijenio na provjeri ispravnosti korisničkog unosa (to obavlja sljedeća funkcija)
#defInputBroj - pretvara upisani string u int/float ovisno o potrebnoj vrsti podataka; ulazni parametri su: strIorF - može poprimiti vrijednost 'f'/'i' i strInputText - tekst upute
def defInputBroj(strIorF, strInputText):
    #try-except - error handling in Python: https://docs.python.org/2/tutorial/errors.html  https://wiki.python.org/moin/HandlingExceptions
    #pokušaj konvertiranja upisanog str u int/float
    try:
        brojUpisano = 0
        
        #konvertiranje upisanog u int
        if strIorF == 'i':
            brojUpisano = int(input(strInputText))
            
        #konvertiranje upisanog u float
        elif strIorF == 'f':
            brojUpisano = float(input(strInputText))
            
        #vraća konvertirani str
        return brojUpisano
    
    #slučaj neuspješnog konvertiranja
    except:
        return 0


def defMain():
    global strGlbScrptpth
    global strGlbVrijemeZapisaBak
    #incijalizacija strGlbVrijemeZapisaBak - vremena backupa na '0', kasnije se pri backupu mijenja u string koji sadržava vrijeme određenog backupa
    strGlbVrijemeZapisaBak = '0'
    
    #strGlbScrptpth - sadržava path ove datoteke (Program.py)
    strGlbScrptpth = os.path.dirname(__file__)
    
    #poziv funkcije defUcitaj radi učitanja podataka o stanovima iz datoteke PopisStanara.txt odmah na početku programa
    defUcitaj()
    
    #poziv funkcije defBrojStanara radi izračuna br. stanara za rasvjetu odnosno lift prema podacima iz učitane datoteke
    defBrojStanara()
    
    #prvi poziv menua - funkcije defMenu
    defMenu()

    
#poziv glavne funkcije programa - defMain
defMain()
